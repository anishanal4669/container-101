### What is a Container?


A container is a lightweight, portable, and executable software package that includes everything needed to run a piece of software, including the code, runtime, libraries, and system tools. Containers provide a consistent and isolated environment for applications, allowing them to run consistently across different computing environments.

In a typical virtualized environment, one or more virtual machines run on top of a physical server using a hypervisor like Xen, Hyper-V, etc.

On the other hand, Containers run on top of the operating system’s kernel. You can call it OS-level virtualization. Before getting into the underlying container concepts, you need to understand two key Linux concepts.

  ![image info](images/image-1.png)


 - **Userspace**: All the code required to run user programs (applications, process) is called userspace. When you initiate a program action, for example, to create a file, the process in the userspace makes a system call to Kernal space.
 - **Kernel Space**: This is the heart of the operating system, where you have the kernel code, which interacts with the system hardware, storage, etc.

### How does containers achieve process isolation and it is ligtweight ?


Containers achieve process isolation through a combination of Linux kernel features and containerization technologies. The primary mechanisms that contribute to container isolation include:

**Linux Namespaces**: Linux namespaces are a kernel feature that allows processes to have their own isolated view of system resources. Containers use several types of namespaces to isolate different aspects of a process's execution environment. Common namespaces include:

 - **PID namespace**: Isolates the process ID number space, ensuring that processes within a container have their unique set of process IDs.

 - **Network namespace**: Provides network isolation, giving each container its own network stack, interfaces, and routing table.

 - **Mount namespace**: Isolates the filesystem mount points, enabling containers to have their own filesystem hierarchy.

 - **IPC namespace**: Separates inter-process communication resources, such as message queues and semaphores.

 - **UTS namespace**: Isolates the hostname and NIS domain name.

**Control Groups (cgroups)**: Cgroups allow the allocation of system resources, such as CPU, memory, and I/O, to processes or groups of processes. Containers use cgroups to limit and control the resource usage of containerized processes. This ensures that a container cannot consume more resources than allocated, preventing resource contention with other containers or the host system.

**Root Filesystem Isolation**: Containers typically use a layered filesystem, and each container has its own root filesystem layer. This ensures that changes made within a container are isolated from the host and other containers. The layered filesystem also allows for the efficient sharing of common layers among multiple containers, reducing storage overhead.

**Seccomp (Secure Computing)**: Seccomp is a security feature that enables the restriction of the system calls available to a process. Containers can use Seccomp to reduce the attack surface by limiting the set of system calls that containerized processes are allowed to make.

**Capabilities**: Linux capabilities provide fine-grained control over privileges. Containers can drop unnecessary capabilities to limit the scope of potential security vulnerabilities. For example, a container may drop the capability to modify the network configuration if it doesn't require that functionality.

### Is Container a Single Process? Can a container have more than one parent process?


No, a container cannot have more than one parent processes. A container runs a single main process, which is started when the container is initiated. The main process is responsible for starting and managing any child processes that the container needs to run.


### Why only one parent process?
Containers are primarily designed to run a single process per container.

The reason for this is to promote isolation and maintain the stability of the container. When a single process is running in a container, any changes or issues related to that process will only affect the container running that process, not other containers or the host system.

For example, let’s say you want to run a web application with a web server and a database. Instead of running the web server and database in a single container, we need to run them in separate containers on the same host.

For instance, if there is a bug or vulnerability in the web server code, it will not affect other containers or the host system, as the web server is isolated within the container.

Similarly, if a change is made to the web server configuration or settings, it will only affect the container running that process and not other containers or the host system. This promotes isolation and maintainability of the container, making it easier to manage and deploy.


### Is it possible to run multiple processes inside a container?
It is possible to run multiple processes inside a container by running a process manager, such as supervisord.

The process manager acts as the main process for the container, and it can spawn and manage multiple child processes. This allows multiple services or processes to be run inside a single container, but they are all managed by a single parent process (the process manager).


### Explain the concepts of storing data in Container.


Storing data in containers involves managing the persistence and accessibility of data used by applications running inside containers. Containers are typically designed to be stateless, meaning they don't store data internally, and their data disappears when the container is removed. However, there are various strategies and mechanisms to handle data storage for containers:

 - **Data Inside Containers (tmpfs):**

While it's generally not recommended for production use, you can store data directly inside a container. This data will be lost when the container is removed or stops.

 - **Bind Mounts:**

Use bind mounts to map a directory on the host machine to a directory inside the container. This way, data can be shared between the container and the host. Changes made in either location are reflected in both.

```
docker run -d -v /host-data:/container-data my-image
```
 - **Volumes:**

Docker volumes provide a more flexible and managed way to persist data. Volumes are independent of the container's lifecycle and can be easily attached to multiple containers.
```
docker volume create my-volume
docker run -d -v my-volume:/container-data my-image
```

### Explain Container networking concepts

Container networking involves managing communication between containers, as well as between containers and the external network. Here are some key concepts related to container networking:

 1. **Docker Networks:**

    - **Bridge Network:** The default network mode for Docker containers is the bridge network. Containers connected to the same bridge network can communicate with each other using container names or container IPs. Docker creates a bridge network named bridge by default.

```
docker network create my-bridge-network
docker run -d --name container1 --network my-bridge-network my-image1
docker run -d --name container2 --network my-bridge-network my-image2
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' container1
docker exec -it container_id sh
curl http://{IPAddress}:80
``` 
   - **Host Network:** Containers share the network namespace with the host machine, which means they can access services on the host using localhost. However, this eliminates network isolation between the host and containers.

```
docker run -d --name container1 --network host my-image1
```

    - **Overlay Network:** Used for communication between containers across multiple Docker hosts in a swarm. It provides multi-host networking for services.

```
docker network create --driver overlay my-overlay-network
docker service create --name service1 --network my-overlay-network my-image1
docker service create --name service2 --network my-overlay-network my-image2
```

    - **Macvlan Network:** Allows containers to have MAC addresses associated with the physical network interface, making them appear as physical devices on the network.

```
docker network create -d macvlan --subnet=192.168.1.0/24 --gateway=192.168.1.1 -o parent=eth0 my-macvlan-network
docker run -d --name container1 --network my-macvlan-network my-image1
```

2. Container-to-Container Communication:

Containers can communicate with each other using their names or container IPs on the same network.
Use DNS resolution or environment variables to facilitate communication between containers. Docker automatically updates the /etc/hosts file within containers.
Port Mapping:

Use port mapping to expose specific ports of a container to the host machine or the external network.
bash
Copy code
docker run -d --name container1 -p 8080:80 my-image1
Docker Compose:


Service Discovery and Load Balancing:

Container orchestration tools like Kubernetes and Docker Swarm provide service discovery and load balancing for containers. This ensures that traffic is distributed across multiple container instances of a service.

Security and Isolation:

Use network namespaces to isolate containers and prevent unauthorized access.
Implement network policies to control traffic between containers and enforce security rules.
Understanding these concepts is crucial for designing and managing containerized applications in various networking scenarios. The choice of networking configurations depends on the specific requirements of your application and the desired level of isolation and communication between containers.
